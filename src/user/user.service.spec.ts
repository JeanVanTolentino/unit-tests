import { BadRequestException } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { isUUID } from "class-validator";
import { UserRepository } from "../auth/repository/user.repository";
import { UserService } from "./user.service"

describe('UserService', () => {
    let service: UserService;
    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                UserService, {
                    provide: UserRepository,
                    useValue: {
                        validateUUID: jest.fn( (userId) => {
                            if(!isUUID(userId)){
                                throw new BadRequestException('user does not exist')
                            }
                        }),
                        find: jest.fn( (condition) => {
                            if(condition.id){
                                if(condition.id==='c968cb13-a42a-4475-962d-4e6c54c9caac'){
                                    return [{id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                            first_name: 'joe',
                                            last_name: 'regan',
                                            email: 'joe@yahoo.com',
                                            username: 'joe123',
                                            created_at: '2021-10-25T16:25:40.397Z'}]
                                } else {
                                    return []
                                }
                            }

                            if(condition.where){
                                if(condition.where[0].first_name._value==='%joe%'){
                                    return [{id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                            first_name: 'joe',
                                            last_name: 'regan',
                                            email: 'joe@yahoo.com',
                                            username: 'joe123',
                                            created_at: '2021-10-25T16:25:40.397Z'}]
                                } else {
                                    return []
                                }
                            }
                        }),
                        getOne: jest.fn( (user) => {
                            if(!user){
                                throw new BadRequestException('user does not exist')
                            }
                    
                            const {id, username, first_name, last_name, email, created_at} = user
                            return {id, username, first_name, last_name, email, created_at}
                        }),
                        getList: jest.fn( (collection)=> {
                            return collection.map(user=>{
                                const {id, first_name, last_name, username, email} = user
                                return {id, first_name, last_name, username, email}
                            })
                        })
                    }
                }
            ]
        }).compile()
        service = module.get<UserService>(UserService)
    })

    describe('getOne', () => {
        it('should return error "user does not exist" if ID is not valid UUID', async () => {
            try {
                await service.getOne('c968cb13-a42a-4475-962d-4e6c54c9caasasd')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('user does not exist')
            }
        })
        
        it('should return error "user does not exist" when ID is not a registered user', async ()=> {
            try {
                await service.getOne('f872873e-68d9-419c-8264-3f4050341c7c')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('user does not exist')
            }
        })
        //gets one user
        it('should get one user when given ID', async () => {
            const result = await service.getOne('c968cb13-a42a-4475-962d-4e6c54c9caac')
            expect(result).toEqual({id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                    first_name: 'joe',
                                    last_name: 'regan',
                                    email: 'joe@yahoo.com',
                                    username: 'joe123',
                                    created_at: '2021-10-25T16:25:40.397Z'})
        })
    })

    describe('getList', ()=>{
        it('should return a list of search results', async ()=>{
            const result = await service.getList({q:'joe'})
            expect(result).toEqual([{id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                    first_name: 'joe',
                                    last_name: 'regan',
                                    email: 'joe@yahoo.com',
                                    username: 'joe123'}])
        })

        it('should return an empty array when search does not match with anything', async () => {
            const result = await service.getList({q:'bla'})
            expect(result).toEqual([])
        })
    })
})