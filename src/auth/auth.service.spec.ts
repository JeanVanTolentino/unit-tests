import { BadRequestException } from "@nestjs/common"
import { JwtService } from "@nestjs/jwt"
import { Test, TestingModule } from "@nestjs/testing"
import passport from "passport"
import { lastValueFrom } from "rxjs"
import { AuthService } from "./auth.service"
import { UserEntity } from "./entity/user.entity"
import { UserRepository } from "./repository/user.repository"

describe('AuthService', ()=>{

    let service: AuthService

    beforeEach(async ()=>{
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AuthService, {
                    provide: UserRepository,
                    useValue: {
                        save: jest.fn( (user) => ({...user, id: '12345'})),
                        findOne: jest.fn( (condition) => {
                            if(condition.where[0].username === 'jean123' || condition.where[1].email === 'jean@yahoo.com'){
                                return {
                                    first_name: 'jean',
                                    last_name: 'tolentino',
                                    email: 'jean@yahoo.com',
                                    username: 'jean123',
                                    password: 'password'
                                }
                            }
                        } ),
                        registrationValidation: jest.fn( (user, email, username) => {
                            if(user){
                                if (user.username===username){
                                    throw new BadRequestException('username already exists')
                                }
                                if(user.email===email){
                                    throw new BadRequestException('email already exists')
                                }
                            }
                        }),
                        userValidation: jest.fn( (user: UserEntity) => {
                            if(!user){
                                throw new BadRequestException('Invalid username or password')
                            }
                        }),
                        passwordValidation: jest.fn( (valid: boolean) => {
                            if(!valid){
                                throw new BadRequestException('Invalid username or password')
                            }
                        }),
                        find: jest.fn( (condition) => {
                            if(condition.username === 'jean123'){
                                return [{
                                    first_name: 'jean',
                                    last_name: 'tolentino',
                                    email: 'jean1@yahoo.com',
                                    username: 'jean123',
                                    password: '$2b$10$T6b3Fe0DHXoaI1.UyKrqsOH2g4ztbNZlST5br7J3LCARfXuMT5MAq',
                                    id: '12345',
                                }]
                            } else {
                                return []
                            }
                        })
                    },
                },
                {
                    provide: JwtService,
                    useValue: {
                        sign: jest.fn( (payload) => {
                            if(payload.username && payload.first_name && payload.last_name){
                                return 'fake.access.token'
                            }
                        })
                    }
                }
            ]
        }).compile()
        service = module.get<AuthService>(AuthService)
    })

    describe('register', ()=>{
        it('should be able to save new user', async ()=>{
            const result = await service.register({
                first_name: 'jean',
                last_name: 'tolentino',
                email: 'jean1@yahoo.com',
                username: 'jean1234',
                password: 'password'
            })
        
            expect(result).toEqual('registered')

        })

        it('should return "username already exists" when username already exists', async ()=>{
            
            try {
                await service.register({
                    first_name: 'jean',
                    last_name: 'tolentino',
                    email: 'jean1@yahoo.com',
                    username: 'jean123',
                    password: 'password'
                })
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('username already exists')
            }

        })

        it('should return "email already exists" when email already exists', async ()=>{
            try {
                await service.register({
                    first_name: 'jean',
                    last_name: 'tolentino',
                    email: 'jean@yahoo.com',
                    username: 'jean1234',
                    password: 'password'
                })
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('email already exists')
            }
        })

    })

    describe('login', ()=>{
        it('should return "Invalid username or password" when username is invalid', async ()=>{
            try{
                await service.login({
                    username: 'jean1234',
                    password: 'password'
                })
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('Invalid username or password')
            }
        })

        it('should return "Invalid username or password" when password is invalid', async ()=>{
            try {
                await service.login({
                    username: 'jean123',
                    password: 'wrongpassword'
                })
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('Invalid username or password')
            }
        })

        it('should return accessToken', async ()=>{
            const accessToken = await service.login({
                username: 'jean123',
                password: 'password'
            })

            expect(accessToken).toEqual({accessToken: "fake.access.token"})
        })
    })
})