import { BadRequestException } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { FriendsRepository } from "../friends/repository/friends.repository";
import { PostService } from "./post.service"
import { CommentRepository } from "./repository/comment.repository";
import { PostRepository } from "./repository/post.repository";

describe('PostService', ()=> {

    let service: PostService;

    beforeEach(async ()=> {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                PostService, {
                    provide: PostRepository,
                    useValue: {
                        save: jest.fn((values)=> {
                            return values
                        }),
                        find: jest.fn((condition)=>{
                            if(condition.id && condition.user){
                                if(condition.id==='ebfed821-8996-45cd-8a08-af280d62caaa' && condition.user==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                    return [{
                                            id: 'ebfed821-8996-45cd-8a08-af280d62caaa',
                                            title: 'this is a post',
                                            description: 'welcome to the post',
                                            user: 'f872873e-68d9-419c-8264-3f4050341c7b',
                                            created_at: '2021-10-25T16:25:40.397Z'
                                            }]
                                } else{
                                    return []
                                }
                            } else {
                                if(condition.id==='ebfed821-8996-45cd-8a08-af280d62caaa'){
                                    return [{
                                            id: 'ebfed821-8996-45cd-8a08-af280d62caaa',
                                            title: 'this is a post',
                                            description: 'welcome to the post',
                                            user: 'f872873e-68d9-419c-8264-3f4050341c7b',
                                            created_at: '2021-10-25T16:25:40.397Z'
                                            }]
                                } else{
                                    return []
                                }
                            }
                        }),
                        delete: jest.fn((condition)=>{
                            if(condition.id==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                return 'deleted'
                            }
                        }),
                        getPost: jest.fn((post,friend,userId)=>{
                            if(!post){
                                throw new BadRequestException('Post does not exist')
                            }
                            if(!friend && post.user!==userId){
                                throw new BadRequestException('You cannot access this post')
                            }
                            return post
                        }),
                        postValidation: jest.fn((post)=>{
                            if(!post){
                                throw new BadRequestException('Post does not exist')
                            }
                        }),
                        postVerification: jest.fn((post)=>{
                            if (!post){
                                throw new BadRequestException('Cannot access this post')
                            } 
                        })

                    }
                },
                {
                    provide: FriendsRepository,
                    useValue: {
                        find: jest.fn((condition)=>{
                            if(condition.user_id==='c968cb13-a42a-4475-962d-4e6c54c9caac' && condition.friend_user_id==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                return [{
                                        id: '123456',
                                        user_id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                        friend_user_id: 'f872873e-68d9-419c-8264-3f4050341c7b'
                                        }]
                            } else {
                                return []
                            }
                        })
                    }
                },
                {
                    provide: CommentRepository,
                    useValue: {
                        commentFilter: jest.fn((userId,post,friend)=>{
                            if(userId!==post.user && !friend){
                                throw new BadRequestException('You cannot comment on this post')
                            }
                        }),
                        commentValidation: jest.fn((comments,postId)=>{
                            if(comments.post_id!==postId){
                                throw new BadRequestException('Incorrect post ID')
                            }
                        }),
                        getComment: jest.fn((friend,comments,user)=>{
                            if (!friend && comments.user_id!==user){
                                throw new BadRequestException('Cannot access this comment')
                            }
                            const {comment} = comments
                            return {comment}
                        }),
                        deleteComment: jest.fn((comment)=>{
                            if (!comment){
                                throw new BadRequestException('Cannot access this comment')
                            } 
                        }),
                        save: jest.fn((values)=>{
                            return values
                        }),
                        delete: jest.fn((condition)=>{

                        }),
                        find: jest.fn(()=>{

                        })
                    }
                }
            ]
        }).compile()
        service = module.get<PostService>(PostService)
    })
    
    describe('createPost', ()=> {
        it('should be able to create new post', ()=> {
            let value = service.createPost({
                                                title: 'this is a post',
                                                description: 'welcome to the post'
                                            }, 'c968cb13-a42a-4475-962d-4e6c54c9caac')
            expect(value).toEqual({
                                    title: 'this is a post',
                                    description: 'welcome to the post',
                                    user: 'c968cb13-a42a-4475-962d-4e6c54c9caac'
                                })
        })
    })

    describe('getPost', ()=> {
        it('should return error "post does not exist" if post ID does not exist.', async ()=> {
            try{
                await service.getPost('ebfed821-8996-45cd-8a08-af280d62caab','c968cb13-a42a-4475-962d-4e6c54c9caac')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('Post does not exist')
            }
        })

        it('should return error "you cannot access this post" if not a friend', async ()=>{
            try {
                await service.getPost('ebfed821-8996-45cd-8a08-af280d62caaa','c968cb13-a42a-4475-962d-4e6c54c9caac')
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('You cannot access this post')
            }
        })

        it('should be able to view a post', async ()=>{
            let value = await service.getPost('ebfed821-8996-45cd-8a08-af280d62caaa','c968cb13-a42a-4475-962d-4e6c54c9caac')
            expect(value).toEqual({
                                    id: 'ebfed821-8996-45cd-8a08-af280d62caaa',
                                    title: 'this is a post',
                                    description: 'welcome to the post',
                                    user: 'f872873e-68d9-419c-8264-3f4050341c7b',
                                    created_at: '2021-10-25T16:25:40.397Z'
                                    })
        })
    })

    describe('deletePost', ()=>{
        it('should return error "Cannot access this post" when user did not make the post', async ()=>{
            try {
                await service.deletePost('ebfed821-8996-45cd-8a08-af280d62caaa','c968cb13-a42a-4475-962d-4e6c54c9caac')
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('Cannot access this post')
            }
        })

        it('should be able to delete own post', async ()=>{
            let value = await service.deletePost('ebfed821-8996-45cd-8a08-af280d62caaa','f872873e-68d9-419c-8264-3f4050341c7b')
            expect(value).toEqual('deleted')
        })
    })

    describe('createComment', ()=>{
        it('should return error "Post does not exist" when the post does not exist', async ()=>{
            try{
                await service.createComment('ebfed821-8996-45cd-8a08-af280d62caac','f872873e-68d9-419c-8264-3f4050341c7b',
                                            {comment:'this is a comment'})
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('Post does not exist')
            }
        })
        it('should return error "you cannot comment on this post" when not friends', async ()=>{
            try{
                await service.createComment('ebfed821-8996-45cd-8a08-af280d62caaa','f872873e-68d9-419c-8264-3f4050341c7b',
                                            {comment:'this is a comment'})
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('You cannot comment on this post')
            }
        })
    })

})