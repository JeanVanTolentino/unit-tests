import { BadRequestException } from "@nestjs/common"
import { Test, TestingModule } from "@nestjs/testing"
import { isUUID } from "class-validator"
import { UserRepository } from "../auth/repository/user.repository"
import { FriendsService } from "./friends.service"
import { FriendsRepository } from "./repository/friends.repository"

describe('FriendsService', ()=> {

    let service: FriendsService

    beforeEach(async ()=> {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                FriendsService, {
                    provide: UserRepository,
                    useValue: {
                        validateUUID: jest.fn( (id) => {
                            if(!isUUID(id)){
                                throw new BadRequestException('user does not exist')
                            }
                        }),
                        find: jest.fn( (condition) => {
                            if(condition.id==='c968cb13-a42a-4475-962d-4e6c54c9caac'){
                                return [{
                                    id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                    first_name: 'joe',
                                    last_name: 'regan',
                                    email: 'joe@yahoo.com',
                                    username: 'joe123',
                                }]
                            } else {
                                return []
                            }
                        }),
                        isUser: jest.fn( (user) => {
                            if(!user){
                                throw new BadRequestException('user doesn\'t exist')
                            }
                        }),
                    }
                },
                {
                    provide: FriendsRepository,
                    useValue: {
                        find: jest.fn( (condition) => {
                            if(condition.friend_user_id && condition.user_id){
                                if(condition.friend_user_id==='c968cb13-a42a-4475-962d-4e6c54c9caac' && condition.user_id==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                    return [{
                                        user_id: 'f872873e-68d9-419c-8264-3f4050341c7b',
                                        friend_user_id: 'c968cb13-a42a-4475-962d-4e6c54c9caac'
                                    }]
                                } else {
                                    return []
                                }
                            }

                            if(condition.user_id){
                                if(condition.user_id==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                    return [{
                                        user_id: 'f872873e-68d9-419c-8264-3f4050341c7b',
                                        friend_user_id: 'c968cb13-a42a-4475-962d-4e6c54c9caac'
                                    }]
                                } else {
                                    return []
                                }
                            }
                        }),
                        isFriend: jest.fn( (friend, friendUserId, userId) => {
                            if(friend){
                                throw new BadRequestException('You are already friends')
                            }
                            if(friendUserId==userId){
                                throw new BadRequestException('You cannot add yourself')
                            }
                        }),
                        save: jest.fn( (condition) => {
                            return {
                                friend_user_id: condition.friend_user_id,
                                user_id: condition.user_id
                            }
                        }),
                        deleteValidation: jest.fn( (friend) => {
                            if(!friend){
                                throw new BadRequestException('deletion request cannot be processed')
                            }
                        }),
                        delete: jest.fn( (condition) => {
                            if(condition.friend_user_id==='c968cb13-a42a-4475-962d-4e6c54c9caac' && condition.user_id==='f872873e-68d9-419c-8264-3f4050341c7b'){
                                return 'deleted'
                            }
                        }),
                    }
                }
            ]
        }).compile()
        service = module.get<FriendsService>(FriendsService)
    })

    describe('addFriend', ()=> {
        it('should return error message "user does not exist" if UUID is not valid', async ()=> {
            try {
                await service.addFriend('c968cb13-a42a-4475-962d-4e6c54c9caa', 'f872873e-68d9-419c-8264-3f4050341c7b')
            } catch (e) {
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('user does not exist')
            }
        })

        it('should return error message "You are already friends" if you are already friends', async ()=> {
            try {
                await service.addFriend('c968cb13-a42a-4475-962d-4e6c54c9caac', 'f872873e-68d9-419c-8264-3f4050341c7b')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('You are already friends')
            }
        })

        it('should return error message "You cannot add yourself" when trying to add self', async ()=> {
            try {
                await service.addFriend('c968cb13-a42a-4475-962d-4e6c54c9caac', 'c968cb13-a42a-4475-962d-4e6c54c9caac')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('You cannot add yourself')
            }
        })

        it('should return error message "user doesn\'t exist" when id does not exist', async ()=> {
            try {
                await service.addFriend('c968cb13-a42a-4475-962d-4e6c54c9caab', 'c968cb13-a42a-4475-962d-4e6c54c9caac')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('user doesn\'t exist')
            }
        })

        it('should be able to add friend', async ()=> {
            const result = await service.addFriend('c968cb13-a42a-4475-962d-4e6c54c9caac', 'f872873e-68d9-419c-8264-3f4050341c7c')
            expect(result).toEqual({friend_user_id:'c968cb13-a42a-4475-962d-4e6c54c9caac', user_id:'f872873e-68d9-419c-8264-3f4050341c7c'})
        })
    })

    describe('deleteFriend', ()=> {
        it('should return error "user does not exist" when friend\'s user ID is not a valid UUID', async ()=>{
            try {
                await service.deleteFriend('c968cb13-a42a-4475-962d-4e6c54c9ca', 'f872873e-68d9-419c-8264-3f4050341c7c')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('user does not exist')
            }
        })

        it('should return error "deletion request cannot be processed" when userID is not a friend', async () => {
            try {
                await service.deleteFriend('c968cb13-a42a-4475-962d-4e6c54c9caac', 'f872873e-68d9-419c-8264-3f4050341c7c')
            } catch (e){
                expect(e).toBeInstanceOf(BadRequestException)
                expect(e.message).toEqual('deletion request cannot be processed')
            }
        })

        it('should be able to delete a friend', async ()=> {
            const result = await service.deleteFriend('c968cb13-a42a-4475-962d-4e6c54c9caac', 'f872873e-68d9-419c-8264-3f4050341c7b')
            expect(result).toEqual('deleted')
        })

    })

    describe('listfriends', ()=> {
        it('should list all your friends', async ()=>{
            const result = await service.listFriends('f872873e-68d9-419c-8264-3f4050341c7b')
            expect(result).toEqual([{id: 'c968cb13-a42a-4475-962d-4e6c54c9caac',
                                    first_name: 'joe',
                                    last_name: 'regan',
                                    email: 'joe@yahoo.com',
                                    username: 'joe123',}])
        })

    })
})